<?php

namespace App\Models;

use App\Core\Model;
use App\Core\Helper;
use App\Core\ModelInterface;

class Post extends Model implements ModelInterface
{
    protected $id;
    /**
     * @ModelRelation(User::class)
     */
    protected $author;
    protected $title;

    public function initRelation(): array {
        return [
            'author' => User::class
        ];
    }


    public function setId(int $id): self
    {
        $this->id=$id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $user): Post
    {
        $this->author = $user;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $user): Post
    {
        $this->author = $user;

        return $this;
    }

}