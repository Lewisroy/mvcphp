<?php 
namespace App\Managers;

use App\Models\Post;
use App\Models\User;
use App\Core\Manager;
use App\Core\Builder\QueryBuilder;
use App\Core\Connection\PDOConnection;

class PostManager extends Manager {

    public function __construct()
    {
        parent::__construct(Post::class, 'post');
    }


    public function getUserPost(int $id = null)
    {


        $query = (new QueryBuilder())
            ->select('p, u')/// p.id as post_id, u.id as user_id,
            ->from('nfoz_post', 'p')
            ->join('nfoz_users', 'u', 'author');
            
            if($id) {
                $query->where('p.author = :iduser')
                ->setParameter('iduser', $id);
            }
           
            return $query->getQuery()
            ->getArrayResult(Post::class)
            ;
        
    }
}

// user id - 1 infoooooPost
// user id - 1 infoooooPost2