<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\User;
use App\Forms\TestType;
use App\Managers\UserManager;

class DefaultController extends Controller
{
    public function defaultAction()
    {
        //Récupéré depuis la bdd
        $firstname = "Yves";
        echo 'Default default';
        //View dashboard sur le template back
       // $myView = new View("dashboard");
        //$myView->assign("firstname", $firstname);
    }

    public function testFormAction()
    {
        $user = (new User())->setFirstName('Fadyl');

        $form = $this->createForm(TestType::class, $user);
        //$form->getBuilder()->remove('firstname')->remove('submit');
        $form->handle();

        if($form->isSubmit() && $form->isValid())
        {  
            //(new UserManager())->save($user);
            // j'ai mon nouveau modele valide ($user) je peux le save
        }

        $this->render("editprofile", "account", [
            "formProfile" => $form
        ]);
    }
}
